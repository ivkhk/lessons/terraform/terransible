variable "pm_api_url" {
    type = string
    default = ""
}

variable "pm_api_token_id" {
    type = string
    default = ""
}

variable "pm_api_token_secret" {
    type = string
    default = ""
}