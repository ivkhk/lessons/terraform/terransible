resource "proxmox_vm_qemu" "vm" {
    name = "VM-Terraform"
    target_node = "pve"
    memory = 512
    cores = 2
    agent = 1
    full_clone = false
    clone = "New-Template"
    boot = "order=scsi0"
    
    network {
        bridge    = "vmbr0"
        firewall  = false
        link_down = false
        model     = "virtio"
    }

    scsihw = "virtio-scsi-single"

    disks {
        scsi {
            scsi0 {
                disk {
                    size = 8
                    storage = "local-lvm"
                }
            }
        }
    }

}